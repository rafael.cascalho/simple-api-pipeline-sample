const http = require('http')
const assert = require('assert')
const app = require('./app')

// Setup
app.listen(8080)

const options = {
    path: '/',
    port: 8080,
    hostname: 'localhost',
}

function reqCallback(res) {
    let serverResponse = ''
    res.on('data', (chunck) => {
        serverResponse += chunck
    })

    // Verify
    res.on('end', () => {
        assert(serverResponse === 'Server Up')
        console.log('[SUCCESS] Server Status is up!')

        // Teardown
        app.close()
    })
}

// Exercise
http.get(options, reqCallback)
