const http = require('http')

module.exports = http.createServer((req, res) => {
    res.write('Server Up')
    res.end()
})
